#include <iostream>
#include <string>
#include <time.h>

#include "Pokemon.h"
#include "Player.h"
#include "PokeParty.h"

using namespace std;


Player::Player()
{
	this->playerName = "";

	this->choice = 0;
	this->move;
	this->consent;

	this->x = 0;
	this->y = 0;
	this->locationNo = 0;

	this->pokeball = 0;
	this->money = 0;
}

Player::Player(string name, int choice, char move, char consent, int x, int y, int locationNo, int pokeball, int money)
{
	this->playerName = name;

	this->choice = choice;
	this->move = move;
	this->consent = consent;

	this->x = x;
	this->y = y;
	this->locationNo = locationNo;

	this->pokeball = pokeball;
	this->money = money;
}


void Player::intro(Player* you)
{
	cout << "Hello Player! Welcome to the world of Pokemon! \n";
	cout << "What is your name? \n";

	cin >> you->playerName;

	you->money = 1000;
	you->pokeball = 10;
}

void Player::areaDisplay(Player * you)
{
	if (you->x >= -1 && you->x <= 1 && you->y <= 1 && you->y >= -1)
	{
		you->locationNo = 2; 
	}
	else if (you->x >= -1 && you->x <= 1 && you->y >= 2 && you->y <= 4)
	{
		you->locationNo = 1;
	}
	else if (you->x >= -1 && you->x <= 1 && you->y >= 5 && you->y <= 7)
	{
		you->locationNo = 3; 
	}
	else if (you->x >= -1 && you->x <= 1 && you->y >= 8 && you->y <= 18)
	{
		you->locationNo = 4;
	}
	else if (you->x >= -1 && you->x <= 1 && you->y >= 19 && you->y <= 21)
	{
		you->locationNo = 5;
	}
	else if (you->x >= 2 && you->x <= 6 && you->y >= 22 && you->y <= 24)
	{
		you->locationNo = 7;
	}
	else if (you->x >= 7 && you->x <= 9 && you->y >= 22 && you->y <= 24)
	{
		you->locationNo = 6;
	}
	else if (you->x >= -1 && you->x <= 1 && you->y >= -13 && you->y <= -11)
	{
		you->locationNo = 8;
	}
	else
	{
		you->locationNo = 0;
	}
}

void Player::runChoice(Player* you, Pokemon* pika, PokeParty* party)
{
	srand(time(NULL));

	int chanceEncounter = 0;
	you->areaDisplay(you);

	string location[9] = {	"Unknown", "Route 1", "Pallet Town", "Viridian City", "Route 2", "Pewter City", "Route 3", "Mt. Moon", "Cinnabar Town" };

	//Choice
	if (you->locationNo == 2 || you->locationNo == 3 || you->locationNo == 5 || you->locationNo == 8)
	{
		cout << "What would you like to do?" << endl;
		cout << "[1] Move \n" << "[2] Pokemons \n" << "[3] Pokemon Center \n" << "[4] PokeMart \n " << endl;
		cin >> you->choice;
		cout << endl;

		if (you->choice != 1 && you->choice != 2 && you->choice != 3 && you->choice != 4)
		{
			while (you->choice != 1 && you->choice != 2 && you->choice != 3 && you->choice != 4)
			{
				cout << "Please try again \n";
				cin >> you->choice;
			}
		}
	}
	else if (you->locationNo == 0 || you->locationNo == 1 || you->locationNo == 4 || you->locationNo == 6 || you->locationNo == 7)
	{
		cout << "What would you like to do?" << endl;
		cout << "[1] Move \n" << "[2] Pokemons \n";
		cin >> you->choice;
		cout << endl;

		if (you->choice != 1 && you->choice != 2)
		{
			while (you->choice != 1 && you->choice != 2)
			{
				cout << "Please try again \n";
				cin >> you->choice;
			}
		}
	}


	//Move
	if(you->choice == 1)
	{
		cout << "Move where? \n";
		cout << "[w] Up		" << "[s] Down \n" "[a] Left	" << "[d] Right \n";
		cin >> you->move;
		cout << endl;

		if (you->move != 'w' && you->move != 'a' && you->move != 's' && you->move != 'd')
		{
			while (you->move != 'w' && you->move != 'a' && you->move != 's' && you->move != 'd')
			{
				cout << "Please try again \n";
				cin >> you->move;

				cout << endl;
			}
		}
		if (you->move == 'w')
		{
			you->y += 1;
			you->areaDisplay(you);
		}
		else if (you->move == 's')
		{
			you->y -= 1;
			you->areaDisplay(you);
		}
		else if (you->move == 'a')
		{
			you->x -= 1;
			you->areaDisplay(you);
		}
		else if (you->move == 'd')
		{
			you->x += 1;
			you->areaDisplay(you);
		}
		
		cout << "Current Location :  " << location[locationNo] << endl;
		cout << "(" << you->x << ", " << you->y << ") \n" << endl;

		system("pause");
		system("cls");

		if (you->locationNo == 0 || you->locationNo == 1 || you->locationNo == 4 || you->locationNo == 6 || you->locationNo == 7)
		{
			chanceEncounter = rand() % 100 + 1;

			if (chanceEncounter > 10)
			{
				pika->battle(pika, party, you);
				system("cls");
			}
		}
	}


	//Party Pokemon
	else if (you->choice == 2) 
	{
		system("cls");

		cout << "PkMn Party: \n" << endl;
		pika->pokemonParty(pika, party);

		system("pause");
		system("cls");
	}


	//Pokemon Center
	else if (you->choice == 3)
	{
		cout << "Nurse Joy: Hello, Welcome to " << location[locationNo] << "'s Pokemon Center! \n";
		std::cin.get();

		cout << "Nurse Joy: Would you like me to heal your pokemon back to perfect health? [y/ n] \n";
		cin >> you->consent;

		if (you->consent != 'y' && you->consent != 'n')
		{
			while (you->consent != 'y' && you->consent != 'n')
			{
				cout << "Please try again \n";
				cin >> you->consent;
			}
		}
		if (you->consent = 'y' && you->consent != 'n')
		{
			cout << "~*Too doo Too doo Too doo Doo!*~ \n";
			party->healParty(party);

			cout << "You have healed your pokemon \n";
			cout << "Nurse Joy: Your pokemon are now in perfect health! We hope to see you again! \n";
		}
		else if (you->consent = 'n'&& you->consent != 'y')
		{
			cout << "We hope to see you again! \n";
		}

		system("pause");
		system("cls");
	}


	//PokeMart
	if (you->choice == 4)
	{
		cout << "[Cash:	P " << you->money << "]" << endl;
		cout << "[Pokeballs: " << you->pokeball << "]" << endl;
		cout << "--------------------------------------------------------------------- \n" << endl;
		cout << "Store Clerk: Welcome to " << location[locationNo] << "'s PokeMart! How may I help you? \n" << endl;
		cout << "[1] Buy Pokeball (P 100) \n" << "[2] Leave \n";

		cin >> you->choice;
		cout << endl;

		if (you->choice != 1 && you->choice != 2)
		{
			while (you->choice != 1 && you->choice != 2)
			{
				cout << "Try again" << endl;
				cin >> you->choice;
			}
		}
		else if (you->choice == 1)
		{
			if (you->money >= 100)
			{
				you->pokeball += 1;
				you->money -= 100;
				cout << "You bought 1 pokeball! \n" << "Store Clerk: We hope to see you again! \n" << endl;
			}
			else
			{
				cout << "You have no more cash left! \n" << endl;
			}
		}

		system("pause");
		system("cls");
	}
}