#pragma once
#include <string>

using namespace std;

class Pokemon;

class PokeParty
{
public:
	PokeParty();
	PokeParty(int hp, int baseHp, int level, int baseDamage, int exp, int expToNextLevel, int pokemonNo, int partySize);
	
	string name[6];
	int partySize[6] = { 0, 0, 0, 0, 0, 0 };
	int hp[6] = { 0, 0, 0, 0, 0, 0 };
	int exp[6] = { 0, 0, 0, 0, 0, 0 };
	int level[6] = { 0, 0, 0, 0, 0, 0 };
	int baseHp[6] = { 0, 0, 0, 0, 0, 0 };
	int baseDamage[6] = { 0, 0, 0, 0, 0, 0 };
	int expToNextLevel[6] = { 0, 0, 0, 0, 0, 0 };
	int pokemonNo[6] = { 0, 0, 0, 0, 0, 0 };

	void savePartyInfo(PokeParty * party, Pokemon* pika);
	void healParty(PokeParty* party);
};