#include <iostream>
#include <string>
#include <time.h>

#include "Pokemon.h"
#include "Player.h"
#include "PokeParty.h"

using namespace std;

Pokemon::Pokemon()
{
	this->name = "";
	this->hp = 0;
	this->baseHp = 0;
	this->level = 0;
	this->baseDamage = 0;
	this->exp = 0;
	this->expToNextLevel = 0;
	this->pokemonNo = 0;
}

Pokemon::Pokemon(string name, int hp, int baseHp, int level, int baseDamage, int exp, int expToNextLevel, int pokemonNo)
{
	this->name = name;
	this->hp = hp;
	this->baseHp = baseHp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;
	this->pokemonNo = pokemonNo;
}

void Pokemon::chooseStarter(Pokemon* pika, PokeParty* party)
{
	int pokeChoice = 0;

	cout << "Choose your new starter! \n";
	cout << "[1] Bulbasaur \n" << "[2] Charmander \n" << "[3] Squirtle \n";
	cin >> pokeChoice;

	if (pokeChoice != 1 && pokeChoice != 2 && pokeChoice != 3)
	{
		while (pokeChoice != 1 && pokeChoice != 2 && pokeChoice != 3)
		{
			cout << "Please try again \n";
			cin >> pokeChoice;
		}
	}
	if (pokeChoice == 1)
	{
		cout << "You chose Bulbasaur! \n" << endl;
		pika->pokemonNo = 2;
	}
	else if (pokeChoice == 2)
	{
		cout << "You chose Charmander! \n" << endl;
		pika->pokemonNo = 3;
	}
	else if (pokeChoice == 3)
	{
		cout << "You chose Squirtle! \n" << endl;
		pika->pokemonNo = 4;
	}

	pika->exp = 0;
	pika->level = 5;
	pika->baseHp = rand() % (30 - 20) + 20;
	pika->hp = baseHp;
	pika->baseDamage = rand() % 12 + 8;
	pika->expToNextLevel = 30;

	pika->addToParty(pika, party);

	cout << "Great! Get ready for your new adventure!" << endl;

	system("pause");
	system("cls");
}

void Pokemon::displayInfo(Pokemon* pika)
{
	cout << pika->name << endl;
	cout << "Lvl	" << pika->level << endl;
	cout << "---------- \n";
	cout << "Hp	" << pika->hp << "/ " << pika->baseHp << endl;
	cout << "Dmg	~" << pika->baseDamage << endl;
	cout << "Exp	" << pika->exp << "/ " << pika->expToNextLevel << endl;
	cout << endl;
}

void Pokemon::pokemonDb(Pokemon* pika)
{
	switch (pika->pokemonNo)
	{
	case 1:
		pika->name = "Pikachu";
		break;

	case 2:
		pika->name = "Bulbasaur";
		break;

	case 3:
		pika->name = "Charmander";
		break;

	case 4:
		pika->name = "Squirtle";
		break;

	case 5:
		pika->name = "Pidgey";
		break;

	case 6:
		pika->name = "Vulpix";
		break;

	case 7:
		pika->name = "Slowpoke";
		break;

	case 8:
		pika->name = "Mew";
		break;

	case 9:
		pika->name = "Mewtwo";
		break;

	case 10:
		pika->name = "Ditto";
		break;

	case 11:
		pika->name = "Eevee";
		break;

	case 12:
		pika->name = "Lapras";
		break;

	case 13:
		pika->name = "Scizor";
		break;

	case 14:
		pika->name = "Gengar";
		break;

	case 15:
		pika->name = "Chansey";
		break;

	default:
		pika->name = "case 0: Missing No.";
		break;
	}
}

void Pokemon::battle(Pokemon* pikaParty, PokeParty* party, Player* you)
{
	unsigned srand(time(NULL));

	for (int y = 0; y < 6; y++)
	{
		if (party->hp[y] != 0)
		{
			pikaParty->pokemonNo = y;
			break;
		}

		else if (party->hp[5] == 0)
		{
			cout << "All pokemon are defeated! Ran away to safety \n" << endl;
		}
	}

	Pokemon* pikaEnemy = new Pokemon();
	bool terminate = false;
	int battleChoice = 0;
	int attackChance = 0;
	int catchChance = 0;

	pikaEnemy->pokemonNo = rand() % (15 - 1) + 1;
	pikaEnemy->baseHp = rand() % (30 - 20) + 20;
	pikaEnemy->hp = pikaEnemy->baseHp;
	pikaEnemy->level = rand() % (10 - 1) + 1;
	pikaEnemy->baseDamage = rand() % 12 + 8;
	pikaEnemy->expToNextLevel = 30;
	pikaEnemy->exp = 0;
	pikaEnemy->pokemonDb(pikaEnemy);

	int x = 0;
	int expGain;

	pikaParty->pokemonNo = party->partySize[x];
	pikaParty->pokemonDb(pikaParty);

	while (true)
	{
		while (pikaParty->hp >= 0 && pikaEnemy->hp >= 0)
		{
			cout << "You have encountered a wild " << pikaEnemy->name << "! \n" << endl;

			pikaEnemy->displayInfo(pikaEnemy);
			cout << endl;

			pikaParty->displayInfo(pikaParty);
			cout << endl;

			cout << "------------------------------- \n" << endl;

			cout << "What would you like to do?  (Pokeballs left: " << you->pokeball << ")" << endl;;
			cout << "[1] Attack	" << "[2] catch	" << "[3] Run Away	" << endl;
			cin >> battleChoice;


			if (battleChoice != 1 && battleChoice != 2 && battleChoice != 3)
			{
				while (battleChoice != 1 && battleChoice != 2 && battleChoice != 3)
				{
					cout << "Please try again \n";
					cin >> battleChoice;
				}
			}
			if (you->pokeball == 0 && battleChoice == 2)
			{
				while (battleChoice == 2 && battleChoice != 1 && battleChoice != 3)
				{
					cout << "You ran out of pokeballs! \n" << "Please try again \n";
					cin >> battleChoice;
				}
			}


			//Attack
			if (battleChoice == 1)
			{
				int hitChance = rand() % 100 + 1;
				system("cls");

				if (hitChance > 20)
				{
					cout << pikaParty->name << " attacked wild " << pikaEnemy->name << endl;
					cout << pikaParty->name << " dealt " << pikaParty->baseDamage << " damage. \n" << endl;
					pikaEnemy->hp -= pikaParty->baseDamage;
					std::cin.get();
				}
				else
				{
					cout << pikaParty->name << " attacked wild " << pikaEnemy->name << endl;
					std::cin.get();
					cout << "..." << endl;
					std::cin.get();
					cout << pikaParty->name << " missed! \n" << endl;
					std::cin.get();
				}

				if (pikaEnemy->hp <= 0)
				{
					expGain = rand() % 8 + 5;

					cout << "Wild " << pikaEnemy->name << " fainted! \n";
					cout << pikaParty->name << " gained " << expGain << " exp. \n";
					pikaParty->exp += expGain;

					terminate = true;
					break;
				}


				int recoilChance = rand() % 100 + 1;
				if (recoilChance > 20)
				{
					cout << "Wild " << pikaEnemy->name << " attacked " << pikaParty->name << endl;
					cout << pikaEnemy->name << " dealt " << pikaEnemy->baseDamage << " damage. \n" << endl;
					pikaParty->hp -= pikaEnemy->baseDamage;
					std::cin.get();
				}
				else
				{
					cout << "Wild " << pikaEnemy->name << " attacked " << pikaParty->name << endl;
					std::cin.get();
					cout << "..." << endl;
					std::cin.get();
					cout << pikaEnemy->name << " missed! \n" << endl;
					std::cin.get();
				}

				if (pikaParty->hp <= 0)
				{
					cout << pikaParty->name << " fainted! Use next pokemon? [y/ n] \n";
					cin >> you->consent;

					if (you->consent != 'y' && you->consent != 'n')
					{
						while (you->consent != 'y' && you->consent != 'n')
						{
							cout << "Please try again \n";
							cin >> you->consent;
						}
					}
					if (you->consent = 'y')
					{
						while (party->partySize[x] != 0)
						{
							x++;

							if (party->partySize[x] != 0 && party->hp[x] != 0)
							{
								pikaEnemy->name = party->name[x];
								pikaParty->pokemonNo = party->partySize[x];
								pikaParty->baseHp = party->baseHp[x];
								pikaParty->hp = party->hp[x];
								pikaParty->level = party->level[x];
								pikaParty->baseDamage = party->baseDamage[x];
								pikaParty->expToNextLevel = party->expToNextLevel[x];
								pikaParty->exp = party->exp[x];
								
								pikaParty->pokemonDb(pikaParty);
								cout << pikaParty->name << ", you're next!" << endl;

								break;
							}

							else if (party->partySize[x] == 0)
							{
								cout << "No more pokemon left to battle! Ran away to safety \n" << endl;
								terminate = true;
							}
						}
					}
					else if (you->consent = 'n')
					{
						cout << "Ran away safely! \n";
						terminate = true;
					}
				}

				system("pause");
				system("cls");
			}


			//Catch
			else if (battleChoice == 2)
			{
				system("cls");

				if (you->pokeball == 0)
				{
					cout << "You ran out of pokeballs! \n" << endl;
					break;
				}

				cout << "You threw a pokeball! \n";
				std::cin.get();
				cout << ". \n";
				std::cin.get();
				cout << ".. \n";
				std::cin.get();
				cout << "... \n";
				std::cin.get();

				catchChance = rand() % 100 + 1;
				
				if (you->pokeball > 0 && catchChance < 30)
				{
					expGain = rand() % 30 + 15;
					party->exp[x] += expGain;

					cout << "You caught " << pikaEnemy->name << "! \n";
					cout << pikaParty->name << " gained " << expGain << " exp \n" << endl;

					pikaParty->pokemonNo = pikaEnemy->pokemonNo;
					pikaParty->level = pikaEnemy->level;
					pikaParty->hp = pikaEnemy->hp;
					pikaParty->baseHp = pikaEnemy->baseHp;
					pikaParty->baseDamage = pikaEnemy->baseDamage;
					pikaParty->exp = pikaEnemy->exp;
					pikaParty->expToNextLevel = pikaEnemy->expToNextLevel;

					pikaParty->addToParty(pikaEnemy, party);

					terminate = true;
				}
				else if (you->pokeball > 0 && catchChance > 29)
				{
					cout << pikaEnemy->name << " got out of the ball!" << endl;
				}

				you->pokeball -= 1;
			}


			//Run Away
			else if (battleChoice == 3)
			{
				cout << "Ran away safely! \n";
				terminate = true;
			}

			if (party->exp[x] >= party->expToNextLevel[x])
			{
				party->level[x]++;
				party->exp[x] = 0 + (party->exp[x] - party->expToNextLevel[x]);
				party->hp[x] = party->hp[x] + (party->hp[x] * 0.15);
				party->baseDamage[x] = party->baseDamage[x] + (party->baseDamage[x] * 0.10);
				party->expToNextLevel[x] = party->expToNextLevel[x] + (party->expToNextLevel[x] * 0.20);
				
				cout << pikaParty->name << " is now level " << party->level[x] << "! \n" << endl;
			}

			system("pause");
			system("cls");

			if (terminate == true)
			{
				break;
			}
		}

		if (terminate == true)
		{
			break;
		}
	}
}

void Pokemon::addToParty(Pokemon * pika, PokeParty* party)
{
	for (int x = 0; x < 6; x++)
	{
		if (party->partySize[x] == 0)
		{
			party->savePartyInfo(party, pika);
			break;
		}
		if (party->partySize[5] != 0)
		{
			cout << "Your party is full, caught pokemon is released back to the wild \n" << endl;

			system("pause");
			break;
		}
	}
}

void Pokemon::pokemonParty(Pokemon * pika, PokeParty* party)
{
	for (int x = 0; x < 6; x++)
	{
		if (party->partySize[x] == 0)
		{
			break;
		}

		pika->pokemonNo = party->partySize[x];
		pika->pokemonDb(pika);
		cout << pika->name << endl;

		cout << "Lvl	" << party->level[x] << endl;
		cout << "---------- \n";
		cout << "Hp	" << party->hp[x] << "/ " << party->baseHp[x] << endl;
		cout << "Dmg	~" << party->baseDamage[x] << endl;
		cout << "Exp	" << party->exp[x] << "/ " << party->expToNextLevel[x] << endl;
		cout << endl;
	}

	system("pause");
	system("cls");
}